package main

import "fmt"

func main() {
	a := make([]int, 0)
	printSlice("a", a)

	b := []int{1, 2, 3, 4, 5}
	printSlice("b", b)

	c := b[:2]
	printSlice("c", c)

	d := c[2:5]
	printSlice("d", d)
}

func printSlice(s string, x []int) {
	fmt.Printf("%s len:%d cap: %d val: %v\n", s, len(x), cap(x), x)
}
