package main

import "fmt"

type Vertex struct {
	X, Y int
}

func main() {
	v := Vertex{1, 2}
	p := &v

	fmt.Println(v)

	(*p).X = 10
	fmt.Println(v)

	p.Y = 20
	fmt.Println(v)

	v1 := Vertex{1, 2}
	v2 := Vertex{Y: 10}
	v3 := Vertex{}
	p = &Vertex{1,2}
	p.X, p.Y = 3, 4

	fmt.Println(v1, v2, v3, p)
}