package main

import (
	"fmt"
	"runtime"
	"time"
)

func main() {
	fmt.Print("Go runs on ")

	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		fmt.Println("%s", os)
	}

	fmt.Println("When is Sunday?")

	today := time.Now().Weekday()

	switch time.Sunday {
	case today + 0:
		fmt.Println("Today")
	case today + 1:
		fmt.Println("Tomorrow.")
	case today + 2:
		fmt.Println("In 2 days")
	default:
		println("Too far away")
	}

	t := time.Now()
	fmt.Println("What a day time?", t.Hour())

	switch {
	case t.Hour() >= 22 || t.Hour() < 5:
		fmt.Println("Good night!")
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good morning!")
	case t.Hour() < 22:
		fmt.Println("Good evening!")
	}
}
