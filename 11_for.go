package main

import (
	"fmt"
	"math"
)

func pow(a, b, lim float64) float64 {
	if v := math.Pow(a, b); v < lim {
		return v
	} else {
		fmt.Printf("%g >= %g; so result = ", v, lim)
	}
	return lim
}

func main() {
	sum := 0

	for i := 0; i < 10; i++ {
		sum += i
	}

	fmt.Println(sum)

	sum = 1

	for sum < 1000 {
		sum += sum
	}

	fmt.Println(sum)
	fmt.Println(pow(3, 2, 10))
	fmt.Println(pow(3, 3, 20))
}
