package main

import "fmt"

func main() {
	var a [2]string
	a[0], a[1] = "some", "strings"
	b := [3]string{"another", "strings", "array"}

	fmt.Println(a, b)

	primes := [6]int{2, 3, 5, 7, 11, 13}
	fmt.Println(primes)

	var s []int = primes[4:]

	fmt.Println(s)

	stringsSlice := b[1:3]

	b[2] = "array!"

	fmt.Println(stringsSlice)
	
	r := []bool{true, false, true, true, false, true}

	fmt.Println(r)

	str := []struct{
		i int
		b bool
	}{
		{2, true},
		{3, false},
		{5, true},
		{7, true},
		{11, false},
		{13 ,true},
	}

	fmt.Println(str)
}
