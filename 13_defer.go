package main

import "fmt"

func deferredHelloWorld() {
	defer fmt.Println(" world!")

	fmt.Print("Hello")
}

func main() {
	deferredHelloWorld()

	fmt.Print("Counting")

	for i := 10; i > 0; i-- {
		defer fmt.Println(i)
	}

	fmt.Println(" done")
}
